﻿using UnityEngine;
using System.Collections;

public class InnerDoorButton : MonoBehaviour
{

    [SerializeField]
    private InnerDoor _door;

    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Vector3.Distance(transform.position, GameManager.Instance.PlayerPosition) <= 3)
            {
                _audioSource.Play();
                _door.Open();
            }
        }
    }
}
