﻿using UnityEngine;
using System.Collections;

public class DoorZone : MonoBehaviour {

    [SerializeField]
    private InnerDoor _door;

    void OnTriggerEnter(Collider zone)
    {
        if (zone.GetComponentInChildren<Enemy>() != null)
            _door.Open();
    }
}
