﻿using UnityEngine;
using System.Collections;

public class InnerDoor : MonoBehaviour
{
    [SerializeField]
    private float _autoCloseTime;
    [SerializeField]
    private AnimationClip _openClip;
    [SerializeField]
    private AnimationClip _closeClip;

    private bool _isOpen;
    private float _timeOut;
    private Animation _animationDoor;
    private AudioSource _audioSource;

    public void Open()
    {
        if (!_isOpen)
        {
            if (!_animationDoor.isPlaying)
            {
                _audioSource.Play();
                _isOpen = true;
                _animationDoor.clip = _openClip;
                _animationDoor.Play();
            }
        }
    }

    private void Close()
    {
        if (!_animationDoor.isPlaying)
        {
            _audioSource.Play();
            _isOpen = false;
            _animationDoor.clip = _closeClip;
            _animationDoor.Play();
            _timeOut = _autoCloseTime;
        }
    }

    private void Start()
    {
        _timeOut = _autoCloseTime;
        _animationDoor = GetComponent<Animation>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (_isOpen)
        {
            if (_timeOut <= 0)
                Close();
            else
                _timeOut -= Time.deltaTime;
        }
    }
}
