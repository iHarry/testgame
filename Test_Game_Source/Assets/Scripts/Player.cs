﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour
{
    public event Action _OnDied;

    [SerializeField]
    private GameObject _laserGun;
    [SerializeField]
    private float _maxHealth;

    private float _currentHealth;
    private LineRenderer _laserLine;
    private Enemy _lastEnemy;

    private void Start()
    {
        _laserLine = _laserGun.GetComponentInChildren<LineRenderer>();
        _laserLine.enabled = false;
        _currentHealth = _maxHealth;
    }

    private void Update()
    {
        if (_currentHealth <= 0)
        {
            Die();
        }

        AudioSource audio = _laserGun.GetComponent<AudioSource>();
        if (Input.GetKey(KeyCode.Mouse0))
        {
            _laserLine.enabled = true;

            if (!audio.isPlaying)
                audio.PlayOneShot(audio.clip);

            Ray ray = new Ray(_laserLine.transform.position, _laserLine.transform.forward);
            RaycastHit hit;

            _laserLine.SetPosition(0, ray.origin);

            if (Physics.Raycast(ray, out hit, 100f))
            {
                _laserLine.SetPosition(1, hit.point);

                var enemy = hit.transform.GetComponentInParent<Enemy>();
                if (enemy != null)
                {
                    _lastEnemy = enemy;
                    enemy.Damage();
                }
                else if (_lastEnemy != null)
                    _lastEnemy.StopDamage();
            }
            else
                _laserLine.SetPosition(1, ray.GetPoint(100));
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            _laserLine.enabled = false;

            if (audio.isPlaying)
                audio.Stop();

            if (_lastEnemy != null)
                _lastEnemy.StopDamage();
        }
    }

    public void Damage()
    {
        _currentHealth -= Time.deltaTime;
    }

    public void StopDamage()
    {
        _currentHealth = _maxHealth;
    }

    private void Die()
    {
        _OnDied();
    }
}
