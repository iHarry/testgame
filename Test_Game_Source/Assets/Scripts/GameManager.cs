﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Player _player;
    [SerializeField]
    private GameObject _enemysConteiner;
    [SerializeField]
    private GameObject _uiMenu;
    [SerializeField]
    private Text _text;

    private Enemy[] _enemies;
    private bool _isPause;
    private bool? _isWin;

    public Player Player
    {
        get
        {
            return _player;
        }
    }

    public Vector3 PlayerPosition
    {
        get
        {
            return _player.GetComponentInParent<Transform>().position;
        }
    }

    public static GameManager Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level1", LoadSceneMode.Single);
        SetGamePause(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        _uiMenu.SetActive(false);

        _player._OnDied += Player_OnDied;

        _enemies = _enemysConteiner.GetComponentsInChildren<Enemy>();
        foreach (var enemy in _enemies)
        {
            enemy._OnDied += Enemy_OnDied;
        }
    }

    private void Player_OnDied()
    {
        _isWin = false;
        StartCoroutine(ShowMenuWithDelayCoroutine("Вы проиграли!", 1));
    }

    private void Enemy_OnDied()
    {
        StartCoroutine(CheckEnemiesWithDelayCoroutine(1));
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_isWin == null)
            {
                _isPause = !_isPause;

                if (_isPause)
                {
                    SetGamePause(true);
                    ShowMenu("Пауза");
                }
                else
                {
                    SetGamePause(false);
                    ShowMenu("");
                }
            }
        }
    }

    private IEnumerator ShowMenuWithDelayCoroutine(string text, float delay)
    {
        yield return new WaitForSeconds(delay);
        SetGamePause(true);
        ShowMenu(text);
    }

    private void ShowMenu(string text)
    {
            _text.text = text;  
    }

    private void SetGamePause(bool isPause)
    {
        if (isPause)
        {
            _player.GetComponentInChildren<FirstPersonController>().enabled = false;
            _player.enabled = false;
            Time.timeScale = 0;

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            _uiMenu.SetActive(true);
        }
        else
        {
            _player.GetComponentInChildren<FirstPersonController>().enabled = true;
            _player.enabled = true;
            Time.timeScale = 1;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            _uiMenu.SetActive(false);
        }
    }

    private IEnumerator CheckEnemiesWithDelayCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        CheckEnemies();
    }

    private void CheckEnemies()
    {
        bool hasEnemy = false;
        foreach (var enemy in _enemies)
        {
            if (enemy != null)
                hasEnemy = true;
        }
        if (!hasEnemy)
        {
            _isWin = true;
            StartCoroutine(ShowMenuWithDelayCoroutine("Поздравляем, вы выиграли!", 1));
        }
    }
}
