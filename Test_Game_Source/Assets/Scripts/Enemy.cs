﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour
{
    public event Action _OnDied;

    [SerializeField]
    private float _maxHealth;
    [SerializeField]
    private float _aggroDistance;
    [SerializeField]
    private float _rndRange;
    [SerializeField]
    private float _normalSpeed;
    [SerializeField]
    private float _aggroSpeed;
    [SerializeField]
    private float _maxAggroTime;
    [SerializeField]
    private GameObject _explosion;

    private NavMeshAgent _agent;
    private Animator _animateEnemy;
    private float _currentHealth;
    private float _currentAggroTime;
    private Vector3 _rndDestination;
    private bool _isMoveToPlayer;
    private bool _isAttack;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animateEnemy = GetComponent<Animator>();
        _currentHealth = _maxHealth;
        _currentAggroTime = 0;
    }

    private void Update()
    {
        if (_currentHealth <= 0)
        {
            Die();
        }
        else
        {
            Move();
        }
        if (_currentAggroTime > 0)
        {
            _currentAggroTime -= Time.deltaTime;
        }
    }

    public void Damage()
    {
        _currentHealth -= Time.deltaTime;
    }

    public void StopDamage()
    {
        _currentHealth = _maxHealth;
    }

    private void Move()
    {
        if (IsStoppingDistance())
        {
            _animateEnemy.Play("creature1Attack1");
            _isAttack = true;
            GameManager.Instance.Player.Damage();
        }
        else
        {
            if (_isAttack)
            {
                GameManager.Instance.Player.StopDamage();
                _isAttack = false;
            }
            if (_currentAggroTime > 0)
            {
                _isMoveToPlayer = true;
                _agent.SetDestination(GameManager.Instance.PlayerPosition);
                _agent.speed = _aggroSpeed;
                _animateEnemy.Play("creature1run");
            }
            else if (_currentHealth < _maxHealth)
            {
                _isMoveToPlayer = true;
                _agent.SetDestination(GameManager.Instance.PlayerPosition);
                _agent.speed = _aggroSpeed;
                _currentAggroTime = _maxAggroTime;
                _animateEnemy.Play("creature1run");
            }
            else if (IsAggroDistance())
            {
                _isMoveToPlayer = true;
                _agent.SetDestination(GameManager.Instance.PlayerPosition);
                _agent.speed = _aggroSpeed;
                _animateEnemy.Play("creature1run");
            }
            else if (_isMoveToPlayer)
            {
                _isMoveToPlayer = false;
                _agent.SetDestination(_rndDestination);
                _agent.speed = _normalSpeed;
                _animateEnemy.Play("creature1walkforward");
            }
            else if (_agent.velocity.magnitude <= 0)
            {
                _rndDestination = new Vector3(UnityEngine.Random.Range(-(_rndRange), _rndRange), transform.position.y, UnityEngine.Random.Range(-(_rndRange), _rndRange));
                _agent.SetDestination(_rndDestination);
                _agent.speed = _normalSpeed;
                _animateEnemy.Play("creature1walkforward");
            }
        }
    }

    private bool IsAggroDistance()
    {
        return Vector3.Distance(transform.position, GameManager.Instance.PlayerPosition) <= _aggroDistance;
    }

    private bool IsStoppingDistance()
    {
        return Vector3.Distance(transform.position, GameManager.Instance.PlayerPosition) <= _agent.stoppingDistance;
    }

    private void Die()
    {
        _OnDied();

        Destroy(gameObject);
        Instantiate(_explosion, transform.position, transform.rotation);
    }
}
